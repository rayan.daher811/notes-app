package org.tsofen.UserManagementServer.BL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.tsofen.UserManagementServer.beans.Note;
import org.tsofen.UserManagementServer.repos.NoteRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class NoteBL {

    @Autowired
    NoteRepository repo;

    /**
     * Get all notes from our data source
     * @return
     */
    public List<Note> getAllNotes() {
        List<Note> target = new LinkedList<Note>();
        repo.findAll().forEach(target::add);
        return target;
    }

    /**
     * Finds a note from our data source
     * @param id
     * @return
     */
    public Optional<Note> findNote(int id) {
        return repo.findById(id);
    }

    /**
     * Deletes the note with the inserted id from our data source
     * @param id
     */
    public void deleteById(int id) {
        repo.deleteById(id);
    }

    /**
     * marks the note with the inserted id as read
     * @param id
     * @return true if the operation is done successfully, otherwise false
     */
    public boolean markNoteAsReadById(int id) {
        Optional<Note> note = findNote(id);

        if (note.isPresent()) {
            note.get().setNote_read(true);
            repo.save(note.get());
            return true;
        } else {
            return false;
        }
    }

    /**
     * Adds a new not to our data source
     * @param newNote
     * @return true if the operation is done successfully, otherwise false
     */
    public boolean addNote(Note newNote) {
        if (repo.save(newNote) != null)
            return true;
        return false;
    }
}
