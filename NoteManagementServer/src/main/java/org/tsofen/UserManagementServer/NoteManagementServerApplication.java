package org.tsofen.UserManagementServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NoteManagementServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(NoteManagementServerApplication.class, args);
	}

}
