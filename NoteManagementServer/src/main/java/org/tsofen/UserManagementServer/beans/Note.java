package org.tsofen.UserManagementServer.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="notes")
public class Note {
	private int id;
	private String title;
	private String body;
	private int priority;
	private boolean note_read;

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	@Column
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	@Column
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}

	@Column
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}

	@Column
	public boolean getNote_read() {
		return note_read;
	}
	public void setNote_read(boolean note_read) {
		this.note_read = note_read;
	}

	@Override
	public String toString() {
		return "Note [id=" + id + ", title=" + title + ", Body=" + body + ", priority=" + priority
				+ ", note_read=" + note_read + "]";
	}
	
	
}
