package org.tsofen.UserManagementServer.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.tsofen.UserManagementServer.BL.NoteBL;
import org.tsofen.UserManagementServer.beans.Note;

import javax.swing.text.html.Option;

@RestController
@RequestMapping("Note")
public class NoteController {

	@Autowired
	NoteBL noteBL;
	@GetMapping("")
	public boolean test()
	{
		return true;
	}

	@GetMapping("add")
	public boolean add(@RequestParam String title, @RequestParam String body, @RequestParam int priority)
	{
		// Validation
		if(priority > 5 || priority < 0 ||
				title == null ||
				body == null)
			return false;

		Note note = new Note();
		note.setTitle(title);
		note.setBody(body);
		note.setPriority(priority);
		note.setNote_read(false);

		return this.noteBL.addNote(note);
	}

	@GetMapping("all")
	public List<Note> getAllNotes()
	{
		return noteBL.getAllNotes();
	}

	@GetMapping("delete")
	public boolean delete(@RequestParam int id)
	{
		Optional<Note> noteToDelete = noteBL.findNote(id);

		// Note is not found
		if(!noteToDelete.isPresent())
			return false;

		noteBL.deleteById(noteToDelete.get().getId());
		return true;
	}

	@GetMapping("read")
	public boolean read(@RequestParam int id)
	{
		Optional<Note> noteToRead = noteBL.findNote(id);

		// Note is not found
		if(!noteToRead.isPresent())
			return false;

		noteBL.markNoteAsReadById(noteToRead.get().getId());
		return true;
	}
}
