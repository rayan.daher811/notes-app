package org.tsofen.UserManagementServer.repos;

import org.springframework.data.repository.CrudRepository;
import org.tsofen.UserManagementServer.beans.Note;

public interface NoteRepository extends CrudRepository<Note, Integer>{
}
