package com.example.keepnotes;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.util.Consumer;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.example.keepnotes.services.INotesService;
import com.example.keepnotes.services.IUserAlertService;
import com.example.keepnotes.services.implementation.NotesServiceSingleton;
import com.example.keepnotes.services.implementation.UserAlertServiceSingleton;

public class MainActivity extends AppCompatActivity {
    private INotesService notesService;
    private IUserAlertService alertService;

    private MainActivity currentActivity = this;
    private ListView contactsListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        notesService = NotesServiceSingleton.getInstance(this.getApplicationContext());
        alertService = UserAlertServiceSingleton.getInstance(this.getApplicationContext());

        contactsListView = findViewById(R.id.notes);

        LoadNotes();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        LoadNotes();
    }

    private void LoadNotes() {
        notesService.getAllNotes(new Consumer<Note[]>() {
            @Override
            public void accept(Note[] notes) {
                if (notes != null) {
                    contactsListView = findViewById(R.id.notes);
                    ListAdapter myAdapter = new NotesAdapter(currentActivity, notes);
                    contactsListView.setAdapter(myAdapter);
                } else {
                    alertService.alert("Sorry, we couldn't reach your notes.");
                }
            }
        });
    }

    public void navigateToNoteAddition(View view) {
        // Starting the note addition activity
        startActivity(new Intent(this, NoteAdditionActivity.class));
    }

}