package com.example.keepnotes.services;

/**
 * Service that is responsible to alert the user properly
 */
public interface IUserAlertService {
    void alert(String message);
}
