package com.example.keepnotes;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.util.Consumer;

import com.example.keepnotes.services.INotesService;
import com.example.keepnotes.services.IUserAlertService;
import com.example.keepnotes.services.implementation.NotesServiceSingleton;
import com.example.keepnotes.services.implementation.UserAlertServiceSingleton;

public class NoteAdditionActivity extends AppCompatActivity {

    private INotesService notesService;
    private IUserAlertService alertService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);
        setContentView(R.layout.note_addition_layout);

        notesService = NotesServiceSingleton.getInstance(this.getApplicationContext());
        alertService = UserAlertServiceSingleton.getInstance(this.getApplicationContext());
    }

    public void addNote(View view) {
        TextView titleView = findViewById(R.id.title);
        TextView bodyView = findViewById(R.id.body);
        TextView priorityView = findViewById(R.id.priority);

        String title = titleView.getText().toString();
        String body = bodyView.getText().toString();
        int priority = Integer.parseInt(priorityView.getText().toString());

        if (priority > 5 || priority < 0)
            alertService.alert("Non valid priority (1 - 5)");
        else {
            notesService.addNewNote(new Note(title, body, priority),
                    new Consumer<Boolean>() {
                        @Override
                        public void accept(Boolean noteAdded) {
                            if (noteAdded) {
                                alertService.alert("Note added successfully.");
                                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                getBaseContext().startActivity(intent);
                            } else {
                                alertService.alert("We couldn't add you new note.");
                            }
                        }
                    });
        }
    }
}
