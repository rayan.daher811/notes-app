package com.example.keepnotes;

public class Note {
    private int id;
    private String title;
    private String body;
    private int priority;
    private boolean note_read;

    public Note(String title, String body, int priority) {
        this.title = title;
        this.body = body;
        this.priority = priority;
    }

    public Note(int id, String title, String body, int priority, boolean note_read) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.priority = priority;
        this.note_read = note_read;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isNote_read() {
        return note_read;
    }

    public void setNote_read(boolean note_read) {
        this.note_read = note_read;
    }
}
