package com.example.keepnotes;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class NotesAdapter extends ArrayAdapter<Note> {
    LayoutInflater inflater;
    Context context;

    public NotesAdapter(Context context, Note[] notes) {
        super(context, 0, notes);
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View layout = this.inflater.inflate(R.layout.note_list_item_pattern, null);
        final Note note = getItem(position);

        // Getting all needed views to fill
        TextView titleView = layout.findViewById(R.id.title);
        final TextView priorityView = layout.findViewById(R.id.priority);
        final TextView note_readView = layout.findViewById(R.id.note_read);

        // Setting the note content
        titleView.setText(note.getTitle());
        priorityView.setText("Priority: " + note.getPriority());
        if(note.isNote_read())
            note_readView.setText("Note has been Read");
        else
            note_readView.setText("Note hasn't been Read");
        // Setting the contact call button
        layout.findViewById(R.id.title).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Navigating to the call view with the inserted number
                        Intent intent = new Intent(context, SingleNoteActivity.class);
                        intent.putExtra("id", note.getId());
                        intent.putExtra("title", note.getTitle());
                        intent.putExtra("body", note.getBody());
                        intent.putExtra("priority", note.getPriority());
                        intent.putExtra("note_read", note.isNote_read());
                        context.startActivity(intent);  // Starting activity without results
                    }
                });

        return layout;
    }
}
