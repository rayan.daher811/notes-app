package com.example.keepnotes.services;

import androidx.core.util.Consumer;

import com.example.keepnotes.Note;

import java.lang.reflect.Method;

/* Represents the service to manage the app's notes */
public interface INotesService {
    /**
     * Successfully reached = initialized notes, otherwise null will be returned
     * @param responseListener
     */
    void getAllNotes(Consumer<Note[]> responseListener);

    /**
     * Added Successfully = true, otherwise false will be returned
     * @param newNote The note to add
     * @param responseListener
     */
    void addNewNote(Note newNote, Consumer<Boolean> responseListener);

    /**
     * Deletes the note with the inserted id
     * @param noteIdToDelete
     * @param responseListener
     */
    void deleteNote(int noteIdToDelete, Consumer<Boolean> responseListener);


    /**
     * Marks the note as read
     * @param noteIdToRead
     * @param responseListener
     */
    void markNoteAsRead(int noteIdToRead, Consumer<Boolean> responseListener);

}
