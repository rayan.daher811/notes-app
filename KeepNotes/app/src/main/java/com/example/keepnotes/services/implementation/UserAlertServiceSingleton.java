package com.example.keepnotes.services.implementation;

import android.content.Context;
import android.widget.Toast;

import com.example.keepnotes.services.IUserAlertService;

public class UserAlertServiceSingleton implements IUserAlertService {
    private static UserAlertServiceSingleton instance;

    private Context context;

    private UserAlertServiceSingleton(Context context) {
        this.context = context;
    }

    public static synchronized UserAlertServiceSingleton getInstance(Context context) {
        if (instance == null) {
            instance = new UserAlertServiceSingleton(context);
        }

        return instance;
    }

    public void alert(String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

}
