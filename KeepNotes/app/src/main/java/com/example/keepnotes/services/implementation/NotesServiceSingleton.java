package com.example.keepnotes.services.implementation;

import android.content.Context;

import androidx.core.util.Consumer;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.keepnotes.Note;
import com.example.keepnotes.services.INotesService;
import com.google.gson.Gson;

/**
 * Represents singleton service to manage the app's notes
 */
public class NotesServiceSingleton implements INotesService {
    private static NotesServiceSingleton instance;

    private RequestQueue requestQueue;
    private String url;

    private NotesServiceSingleton(Context context) {
        // getApplicationContext() is key, it keeps you from leaking the
        // Activity or BroadcastReceiver if someone passes one in.
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        url = "http://192.168.43.5:8080/";
    }

    public static synchronized NotesServiceSingleton getInstance(Context context) {
        if (instance == null) {
            instance = new NotesServiceSingleton(context);
        }
        return instance;
    }

    @Override
    public void getAllNotes(final Consumer<Note[]> responseListener) {
        // Getting all notes from our server
        requestQueue.add(new StringRequest(Request.Method.GET, url + "Note/all",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Gson gson = new Gson();
                        responseListener.accept(gson.fromJson(response, Note[].class));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                responseListener.accept(null);
            }
        }));
    }

    @Override
    public void addNewNote(Note newNote, final Consumer<Boolean> responseListener) {
        // Adding the new note
        requestQueue.add(new StringRequest(Request.Method.GET, url + "Note/add/?" +
                "title=" + newNote.getTitle() + "&body=" + newNote.getBody() + "&priority=" + newNote.getPriority(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        responseListener.accept(response.equals("true"));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        responseListener.accept(false);
                    }
                }));
    }

    @Override
    public void deleteNote(int noteIdToDelete, final Consumer<Boolean> responseListener) {
        // Deleting the note
        requestQueue.add(new StringRequest(Request.Method.GET, url + "Note/delete/?id=" + noteIdToDelete,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        responseListener.accept(response.equals("true"));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                responseListener.accept(false);
            }
        }));
    }

    @Override
    public void markNoteAsRead(int noteIdToRead, final Consumer<Boolean> responseListener) {
        // Mark note as read
        requestQueue.add(new StringRequest(Request.Method.GET, url + "Note/read/?id=" + noteIdToRead,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        responseListener.accept(response.equals("true"));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                responseListener.accept(false);
            }
        }));
    }
}
