package com.example.keepnotes;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.util.Consumer;

import com.example.keepnotes.services.INotesService;
import com.example.keepnotes.services.IUserAlertService;
import com.example.keepnotes.services.implementation.NotesServiceSingleton;
import com.example.keepnotes.services.implementation.UserAlertServiceSingleton;

public class SingleNoteActivity extends AppCompatActivity {
    private INotesService notesService;
    private IUserAlertService alertService;

    private int noteId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_note_layout);

        // Initializing services
        notesService = NotesServiceSingleton.getInstance(this.getApplicationContext());
        alertService = UserAlertServiceSingleton.getInstance(this.getApplicationContext());


        // Getting the activity essential data
        Intent intent = getIntent();
        String title = intent.getExtras().getString("title");
        String body = intent.getExtras().getString("body");
        noteId = intent.getExtras().getInt("id");
        int priority = intent.getExtras().getInt("priority");

        // Marking the current note as read
        notesService.markNoteAsRead(noteId, new Consumer<Boolean>() {
            @Override
            public void accept(Boolean noteReadSuccessfully) {
                if(!noteReadSuccessfully)
                    alertService.alert("We couldn't mark the current note as read.");
            }
        });

        TextView titleView = findViewById(R.id.title);
        TextView bodyView = findViewById(R.id.body);
        TextView priorityView = findViewById(R.id.priority);

        titleView.setText("" + title);
        bodyView.setText("" + body);
        priorityView.setText("" + priority);
    }

    public void deleteNote(View view) {
        notesService.deleteNote(noteId, new Consumer<Boolean>() {
            @Override
            public void accept(Boolean deletedSuccessfully) {
                if(deletedSuccessfully){
                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getBaseContext().startActivity(intent);
                    alertService.alert("Note deleted successfully.");
                } else {
                    alertService.alert("We couldn't delete your note.");
                }
            }
        });
    }
}
